package hamster.hamsterstestapp;

import android.app.*;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.*;

public class MainActivity extends AppCompatActivity implements FragmentsBtnPlusListener, FragmentsBtnMinusListener, FragmentsBtnNotificationListener {

    ViewPagerAdapter viewPagerAdapter;
    ViewPager viewPager;
    NotificationManager notificationManager;
    SharedPreferences sharedPreferences;
    int count, count2;

    public static final String NOTIFICATION_INTENT_EXTRAS = "extra";
    public static final String NOTIFICATION_BUILDER_EXTRAS = "pageNumber";
    public static final String NOTIFICATION_CHANNEL_ID = "id";
    public static final String PREFERENCES_PAGE_NUMBERS_LIST_KEY = "key";
    public static final String PREFERENCES_CURRENT_ITEM_KEY = "currentItem";


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewpager); //ViewPager - дозволяє організувати перегляд даних зі свайпанням сторінок вліво-вправо.
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);

        setupViewPager();
    }

    private void setupViewPager() {
        sharedPreferences = getPreferences(MODE_PRIVATE);

        if (sharedPreferences.contains(PREFERENCES_PAGE_NUMBERS_LIST_KEY)) { //ЯКЩО В SharedPreferences ЩОСЬ ЗБЕРЕЖЕНО - ТО ЗАПОВНЮЄМО viewPagerAdapter ЦИМИ ДАНИМИ.

            //переводим отриманий з sharedPreferences LinkedHashSet в TreeSet (щоб дані були правильно посортовані)
            Set<String> textSet = sharedPreferences.getStringSet(PREFERENCES_PAGE_NUMBERS_LIST_KEY, new LinkedHashSet<String>());
            Set<String> textTreeSet = new TreeSet<>(textSet);

            //Переводимо наш <String> Set в <Integer>
            Set<Integer> textTreeSetInteger = new TreeSet<>();
            for (String i : textTreeSet) {
                int intI = Integer.valueOf(i);
                textTreeSetInteger.add(intI);
            }

            //Створюємо фрагменти зі значеннями, взятими зі списку, отриманого з sharedPreferences. Та додаємо їх в viewPagerAdapter.
            for (int i : textTreeSetInteger) {
                MyFragment myFragment = new MyFragment();
                Bundle args = new Bundle();
                args.putInt(MyFragment.PAGENUMBER_ARGS, i);
                myFragment.setArguments(args);

                viewPagerAdapter.addFragment(myFragment);
            }

            //відкриваємо сторінку з фрагментом, на якому завершилась попередня робота з додатком
            int currentItem = sharedPreferences.getInt(PREFERENCES_CURRENT_ITEM_KEY, 0);
            viewPager.setCurrentItem(currentItem);

        } else { //ЯКЩО ДОДАТОК ВІДКРИВАЄТЬСЯ З НУЛЯ - ТО СТВОРЮЄМО ФРАГМЕНТ №1. ТА ДОДАЄМО ЙОГО У viewPagerAdapter.
            MyFragment myFragment = new MyFragment();
            //передаємо фрагменту текст, який він має відобразити
            Bundle args = new Bundle();
            args.putInt(MyFragment.PAGENUMBER_ARGS, 1);
            myFragment.setArguments(args);

            viewPagerAdapter.addFragment(myFragment);
        }
    }

    @Override
    public void btnPlusInFragmentClicked() {
        //визначаємо текст, який має відобразити стрюваний фрагмент
        int lastPagesPosition = viewPagerAdapter.getCount() - 1;
        MyFragment lastPagesFragment = viewPagerAdapter.getItem(lastPagesPosition); //отримуємо останній у списку фрагмент
        String lastPagesText = lastPagesFragment.getTextFromArgs();//отримуємо текст з останнього у списку фрагменту
        int argsForNewFragment = Integer.valueOf(lastPagesText) + 1; //текст, який має відобразити створюваний фрагмент

        //додаємо створюваний фрагмент у viewPager
        MyFragment myNewFragment = new MyFragment();
        Bundle args = new Bundle();
        args.putInt(MyFragment.PAGENUMBER_ARGS, argsForNewFragment);
        myNewFragment.setArguments(args); //передаємо створюваному фрагменту текст, який він має відображати
        viewPagerAdapter.addFragment(myNewFragment);

        //встановлюємо currentItem (фрагмент, який має відображатись на екрані): від к-ті фрагментів в списку адаптера віднімаємо одиницю = currentItem
        int listSize = viewPagerAdapter.getCount();
        int currentItem = listSize - 1;
        viewPager.setCurrentItem(currentItem);
    }

    @Override
    public void btnMinusInFragmentClicked() {

        //ВИДАЛЯЄМО ДАНИЙ ФРАГМЕНТ (на якому клікнули btnMinus). І ВІДОБРАЖАЄМО НА ЕКРАНІ ПОПЕРЕДНІЙ ПО СПИСКУ ФРАГМЕНТ.
        MyFragment currentFragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
        int previousFragmentPosition = viewPagerAdapter.deleteFragment(currentFragment); //м. deleteFragment видаляє даний фрагмент і повертає позицію попереднього в списку фрагменту (який треба відобразити на екрані)
        viewPagerAdapter.notifyDataSetChanged(); //make adapter redraw itself
        viewPager.setCurrentItem(previousFragmentPosition - 1); //від попереднього від видаленого фрагменту віднімаємо одницю = currentItem

        //ВИДАЛЯЄМО NOTIFICATIONS, ЯКІ БУЛИ ВИКЛИКАНІ З ДАНОГО ФРАГМЕНТУ (тобто, extras яких співпадає з текстом даного фрагменту).
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        StatusBarNotification[] activeNotifications = notificationManager.getActiveNotifications();

        //проходимось по списку активних notifications: отримуємо їхні extras та id
        for (int i = 0; i < activeNotifications.length; i++) {
            String extras = activeNotifications[i].getNotification().extras.getString(NOTIFICATION_BUILDER_EXTRAS);
            int id = activeNotifications[i].getId();

            //якщо extras даного notification співпадає з текстом даного фрагменту - то видаляємо цей notification по його id
            if (extras.equals(currentFragment.getTextFromArgs())) {
                notificationManager.cancel(id);
            }
        }
    }

    @Override
    public void btnNotificationClicked(String pageNumber) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_LAUNCHER);

        //Поміщаємо в Intent pageNumber, на якому створюємо notification (щоб при кліку на notification показати саме цей фрагмент)
        notificationIntent.putExtra(NOTIFICATION_INTENT_EXTRAS, pageNumber);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, count, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        count = count + 1; //щоб requestCode був іншим щоразу при створенні PendingIntent

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuildWithChannel(pageNumber, pendingIntent);
        } else {
            notificationBuildNormal(pageNumber, pendingIntent);
        }
    }

    private void notificationBuildNormal (String pageNumber, PendingIntent pendingIntent) {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Bundle extras = new Bundle();
        extras.putString(NOTIFICATION_BUILDER_EXTRAS, pageNumber);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("You create a notification")
                .setContentText("Notification " + pageNumber)
                .addExtras(extras); //передаємо extras з текстом даного фрагменту (знадобиться для коректного видалення створеного notification)

        Notification notification = builder.build();
        notificationManager.notify(count2, notification);

        count2 = count2 + 1; //щоб id стврюваних notification було щоразу іншим
    }

    private void notificationBuildWithChannel(String pageNumber, PendingIntent pendingIntent) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel mChannel = null;

            mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_HIGH);

            Bundle extras = new Bundle();
            extras.putString(NOTIFICATION_BUILDER_EXTRAS, pageNumber);

            Notification.Builder builder = new Notification.Builder(this);

            builder.setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setChannelId(NOTIFICATION_CHANNEL_ID)
                    .setContentTitle("You create a notification")
                    .setContentText("Notification " + pageNumber)
                    .addExtras(extras); //передаємо extras з текстом даного фрагменту (знадобиться для коректного видалення створеного notification)

            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);

            Notification notification = builder.build();
            notificationManager.notify(count2, notification);

            count2 = count2 + 1; //щоб id стврюваних notification було щоразу іншим
        }
    }

    //Ловимо intent, від notification
    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        String pageNumber = extras.getString(NOTIFICATION_INTENT_EXTRAS); //отримуємо з Intent pageNumber фрагменту, на якому був створений даний notification.
        int currentFragmentIndex = viewPagerAdapter.getFragmentIndexByPageNumber(pageNumber); //по цьому pageNumber отримуємо індекс в списку даного фрагменту.
        viewPager.setCurrentItem(currentFragmentIndex); //по індексу відкриваємо сurrentItem на екрані.
    }

    @Override
    protected void onStop() {
        super.onStop();
        Set<String> fragmentsPageNumbersList = viewPagerAdapter.getFragmentsPageNumbersList();

        sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(PREFERENCES_PAGE_NUMBERS_LIST_KEY, fragmentsPageNumbersList);
        editor.putInt(PREFERENCES_CURRENT_ITEM_KEY, viewPager.getCurrentItem());

        editor.commit();
    }

}
