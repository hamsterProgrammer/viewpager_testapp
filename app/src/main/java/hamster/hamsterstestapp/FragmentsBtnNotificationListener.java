package hamster.hamsterstestapp;

public interface FragmentsBtnNotificationListener {
    void btnNotificationClicked (String pageNumber);
}
