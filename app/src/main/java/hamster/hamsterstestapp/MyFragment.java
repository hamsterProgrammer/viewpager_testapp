package hamster.hamsterstestapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


public class MyFragment extends Fragment implements View.OnClickListener {

    private TextView tvCount;
    private Button btnNotification;
    private ImageButton btnMinus, btnPlus;
    FragmentsBtnPlusListener fragmentsBtnPlusListener;
    FragmentsBtnMinusListener fragmentsBtnMinusListener;
    FragmentsBtnNotificationListener fragmentsBtnNotificationListener;

    public static final String PAGENUMBER_ARGS = "args";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentsBtnPlusListener = (FragmentsBtnPlusListener) context; //Кастуємо activity (яке отримуємо на вхід onAttach) до типу інтерфейсу FragmentsBtnPlusListener (щоб можна було викликати його м. btnPlusInFragmentClicked, реалізований в MainActivity).
        fragmentsBtnMinusListener = (FragmentsBtnMinusListener) context;
        fragmentsBtnNotificationListener = (FragmentsBtnNotificationListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.my_fragment, container, false);
        tvCount = (rootView.findViewById(R.id.txtCount));
        btnPlus = rootView.findViewById(R.id.btnPlus);
        btnMinus = rootView.findViewById(R.id.btnMinus);
        btnNotification = rootView.findViewById(R.id.btnNotification);
        btnPlus.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnNotification.setOnClickListener(this);

        Bundle arguments = getArguments();
        int pageNumber = arguments.getInt("args");

        if (pageNumber == 1) {
            btnMinus.setVisibility(View.INVISIBLE);
        }

        String pageNumberStr = Integer.toString(pageNumber);
        tvCount.setText(pageNumberStr);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPlus:
                fragmentsBtnPlusListener.btnPlusInFragmentClicked();
                break;
            case R.id.btnMinus:
                fragmentsBtnMinusListener.btnMinusInFragmentClicked();
                break;
            case R.id.btnNotification:
                String pageNumber = getTextFromArgs();
                fragmentsBtnNotificationListener.btnNotificationClicked(pageNumber);
                break;
        }
    }

    //Повертає текст аргументу даного фрагменту (тобто, його pageNumber)
    public String getTextFromArgs() {
        Bundle arguments = getArguments();
        int pageNumber = arguments.getInt("args");
        String pageNumberStr = Integer.toString(pageNumber);

        return pageNumberStr;
    }

}
