package hamster.hamsterstestapp;

import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.*;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<MyFragment> fragmentList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override //повертає елемент нашего списка по позиції
    public MyFragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override //повертає к-кість елементів у нашому списку
    public int getCount() {
        return fragmentList.size();
    }

    //Перевизначаємо метод адаптера getItemPosition (щоб можна було корректно видаляти фрагменти при кліку на кн. мінус)
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    //Додає фрагмент (який іде на вхід м-ду) в fragmentList.
    public void addFragment(MyFragment fragment) {
        fragmentList.add(fragment);
        notifyDataSetChanged();
    }

    //Видаляє фрагмент (який іде на вхід м-ду) з fragmentList. Та повертає позицію попереднього по списку фрагменту (щоб відобразити його на екрані)
    public int deleteFragment(MyFragment fragment) {
        int previousFragmentPosition = fragmentList.indexOf(fragment);//отримуємо позицію попереднього фрагменту (який іде перед фрагментом, який хочемо видалити)
        fragmentList.remove(fragment);

        return previousFragmentPosition;
    }

    //Повертає set з id-шками (тобто, текстом) всіх фрагментів, що є у fragmentList.
    public Set<String> getFragmentsPageNumbersList() {

        Set<String> textList = new LinkedHashSet<>();

        for (int i = 0; i < fragmentList.size(); i++) {
            MyFragment myFragment = fragmentList.get(i);
            String fragmentText = myFragment.getTextFromArgs();
            textList.add(fragmentText);
        }

        return textList;
    }

    //Повертає індекс фрагменту в fragmentList за його PageNumberом
    public int getFragmentIndexByPageNumber(String pageNumber) {
        MyFragment currentFragment = new MyFragment();
        int currentFragmentIndex = 0;

        for (int i = 0; i < fragmentList.size(); i++) {
            MyFragment myFragment = fragmentList.get(i);
            String myFragmentPageNumber = myFragment.getTextFromArgs();

            if (pageNumber.equals(myFragmentPageNumber)) {
                currentFragment = myFragment;
                currentFragmentIndex = fragmentList.indexOf(currentFragment);
            }
        }

        return currentFragmentIndex;
    }

}
